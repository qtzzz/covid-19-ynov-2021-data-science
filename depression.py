from bokeh.layouts import column
from bokeh.models import ColumnDataSource, FactorRange
from bokeh.plotting import figure
from bokeh.transform import factor_cmap

import tools.data_provision


def reformat_data(df):
    columns = df.columns.to_list()
    columns_values = {}
    for i in range(len(columns)):
        columns_values[columns[i]] = df[columns[i]].to_list()

    return columns_values


def reformat_dico(df):
    date = df["Date"].to_list()
    jeune = df["18-24 ans"].to_list()
    moyen = df["25-29 ans"].to_list()
    vieux = df["30 ans ou plus"].to_list()
    dico = {}
    for i in range(len(date)):
        row = [jeune[i], moyen[i], vieux[i]]
        dico[date[i]] = row
    return (dico)


def show_bar():
    depression = tools.data_provision.DataProvider('AgeSyndromeDepressif', "xlsx").load_cleaned_pandas_dataset()
    data = reformat_data(depression)
    dico = reformat_dico(depression)

    keys = list(data.keys())
    dates = keys[1:]
    categories = data[keys[0]]
    print(categories)
    print(data)

    palette = ["#c9d9d3", "#718dbf", "#e84d60"]

    x = [(date, categorie) for date in dates for categorie in categories]
    counts = sum(zip(dico[categories[0]], dico[categories[1]], dico[categories[2]]), ())  # like an hstack

    source = ColumnDataSource(data=dict(x=x, counts=counts))

    p = figure(x_range=FactorRange(*x), height=500, title="Dépression  ",
               toolbar_location=None, tools="")

    p.vbar(x='x', top='counts', width=0.9, source=source, line_color="white",
           fill_color=factor_cmap('x', palette=palette, factors=categories, start=1, end=2))

    p.y_range.start = 0
    p.x_range.range_padding = 1
    p.xaxis.major_label_orientation = 1
    p.xgrid.grid_line_color = None

    return p


def print_heure_diff() -> figure:
    p = show_bar()
    return p

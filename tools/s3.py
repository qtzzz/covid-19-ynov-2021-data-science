"""
The s3 module provides functions intended to interact with the project cloud storage

IMPORTANT: The credentials file should be located at the root of the project or the
environment variable 'GOOGLE_APPLICATION_CREDENTIALS' should be manually exported with
a value corresponding to the absolute path of the credentials file
"""

import io
import os
from copy import Error

from google.cloud import storage


if not os.getenv("GOOGLE_APPLICATION_CREDENTIALS"):
    os.putenv("GOOGLE_APPLICATION_CREDENTIALS", os.path.join(os.getcwd(), "sa.json"))


class DatasetNotFoundError(Exception):
    """
    Exception raised when the dataset cannot be found on s3
    """
    pass


def get_object(object_key: str) -> io.BytesIO:
    """
    Download object from google cloud storage bucket
    
    :param object_key: The key of the object to be retrieved from cloud storage
    :return: Bytestring containing the retrieved object
    :raise DatasetNotFoundError: If dataset was not found
    """
    storage_client = storage.Client()
    bucket_name = "ynov-projet-covid-datascience"
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.get_blob(object_key)
    if not blob:
        raise DatasetNotFoundError(object_key)
    return io.BytesIO(blob.download_as_string())


def upload_object(object_key: str, file_to_upload: str):
    """
    Upload object to google cloud storage bucket
    
    :param object_key: The key of the object to be uploaded from cloud storage
    :param file_to_upload: file to upload
    """
    storage_client = storage.Client()
    bucket = storage_client.get_bucket("ynov-projet-covid-datascience")
    blob = bucket.blob(object_key)
    try:
        blob.upload_from_string(file_to_upload)
    except Error as e:
        print(e)
    
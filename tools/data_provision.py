"""
The data_provision module provides a convenience class 'DataProvider' that should be used to interact with the needed
datasets
"""

import bokeh.models
import pandas as pd

import tools.s3


class DataProvider:
    """
    Convenience class that should be used to load and save datasets
    """

    def __init__(self, name: str, extension="csv"):
        """
        Initialize base class parameters

        :param name: The name of the dataset without its extension
        :param extension: The extension of the dataset (ex: csv, tsv, ...)
        """
        self.name = name

        if extension.startswith("."):
            self.extension = extension
        else:
            self.extension = "." + extension

    def load_raw_dataset(self, fig="",) -> pd.DataFrame:
        """
        Loads the uncleaned version of the dataset as a pandas DataFrame

        :return: Pandas DataFrame corresponding to the uncleaned dataset
        or empty DataFrame if the dataset could not be loaded
        """
        try:
            dataset = tools.s3.get_object("raw/" + self.name + self.extension)
        except tools.s3.DatasetNotFoundError:
            return pd.DataFrame()

        if self.extension == ".tsv":
            return pd.read_csv(dataset, sep="\t")

        elif self.extension == "xlsx":
            return pd.read_excel(dataset, fig)

        return pd.read_csv(dataset, index_col=0)
    
    
    def load_file(self) -> pd.DataFrame:
        try:
            dataset = tools.s3.get_object("raw/" + self.name + self.extension)
        except tools.s3.DatasetNotFoundError:
            return 
        return dataset

    def load_cleaned_bokeh_dataset(self) -> bokeh.models.ColumnDataSource:
        """
        Loads the cleaned version of the dataset as a bokeh ColumnDataSource

        :return: Bokeh ColumnDataSource corresponding to the cleaned dataset
        or empty ColumnDataSource if the dataset could not be loaded
        """
        try:
            dataset = tools.s3.get_object(
                "cleaned/" + self.name + self.extension)
        except tools.s3.DatasetNotFoundError:
            return bokeh.models.ColumnDataSource()

        return bokeh.models.ColumnDataSource(pd.read_csv(dataset))

    def load_cleaned_pandas_dataset(self) -> pd.DataFrame:
        """
        Loads the cleaned version of the dataset as a pandas DataFrame

        :return: pandas DataFrame corresponding to the cleaned dataset
        or empty DataFrame if the dataset could not be loaded
        """
        try:
            dataset = tools.s3.get_object(
                "cleaned/" + self.name + self.extension)
        except tools.s3.DatasetNotFoundError:
            return pd.DataFrame()

        return pd.read_csv(dataset, index_col=0)

    def save_cleaned_dataset(self, dataset: pd.DataFrame):
        """
        Saves the cleaned dataset so that it can be later retrieved

        :param dataset: The dataset to save
        """
        tools.s3.upload_object("cleaned/" + self.name +
                               self.extension, dataset.to_csv())

"""
The choropleth module provide functions to create choropleth maps for bokeh
"""
import json
from bokeh.core.property.dataspec import value
from functools import partial
import geopandas as gpd
import numpy as np
import pandas as pd
from bokeh.io.doc import curdoc
from bokeh.layouts import widgetbox, column, Row
from bokeh.models import GeoJSONDataSource, LinearColorMapper, ColorBar, Div, Button
from bokeh.models import Slider, HoverTool
from bokeh.palettes import brewer, mpl
from bokeh.plotting import figure
from bokeh.events import Tap
import tools.data_provision
import time


data_provider = tools.data_provision.DataProvider('covid_departement')
df_dept = data_provider.load_cleaned_pandas_dataset()
gdf = gpd.read_file("./assets/departements-20180101.shp")
gdf.drop(gdf.index[gdf['code_insee'] == '972'], inplace=True)
gdf.drop(gdf.index[gdf['code_insee'] == '971'], inplace=True)
gdf.drop(gdf.index[gdf['code_insee'] == '973'], inplace=True)
gdf.drop(gdf.index[gdf['code_insee'] == '974'], inplace=True)
gdf.drop(gdf.index[gdf['code_insee'] == '975'], inplace=True)
gdf.drop(gdf.index[gdf['code_insee'] == '976'], inplace=True)
df_dept.drop(df_dept.index[df_dept['departement'] == '972'], inplace=True)
df_dept.drop(df_dept.index[df_dept['departement'] == '971'], inplace=True)
df_dept.drop(df_dept.index[df_dept['departement'] == '973'], inplace=True)
df_dept.drop(df_dept.index[df_dept['departement'] == '974'], inplace=True)
df_dept.drop(df_dept.index[df_dept['departement'] == '975'], inplace=True)
df_dept.drop(df_dept.index[df_dept['departement'] == '976'], inplace=True)
df_dept = df_dept.rename(columns={'departement': 'code_insee'})
df_dept['date'] = pd.to_datetime(df_dept['date'])
df_dept['date'] = pd.to_datetime(df_dept.date).dt.to_period('m').dt.strftime('%Y-%m')
df_dept = df_dept.groupby(['date', 'code_insee'])[['nb_patients_hosp']].sum().reset_index()

dates = df_dept['date'].unique()

df_dept.date = df_dept['date'].astype(np.datetime64)

placeholder_figure = figure(title='Nombres d\'hospitalisations mensuelles depuis Mars 2020',
               plot_height=650, plot_width=1000,
               toolbar_location=None,
               output_backend="webgl")



def json_data(selectedYear):
    yr = selectedYear
    df_yr = df_dept[df_dept['date'] == dates[int(yr) - 1]]
    merged = gdf.merge(df_yr, on='code_insee')
    merged['date'] = merged['date'].astype(str)
    merged_json = json.loads(merged.to_json()) #5s
    json_data = json.dumps(merged_json) #2,3s
    return json_data, df_yr


def update_plot(attr, old, new):
    layout.children[0] = placeholder_figure
    yr = slider.value
    new_data, df_yr = json_data(yr)
    p = make_plot(input_field, df_yr)
    new_layout = column(p)
    layout.children[0] = new_layout
    geosource.geojson = new_data

def make_plot(field_name, df_yr):

    # palette = mpl['Plasma256'][7]
    # palette = palette[::-1]
    color_mapper = LinearColorMapper(palette='Plasma256', low=df_yr['nb_patients_hosp'].min(),
                                     high=df_yr['nb_patients_hosp'].max())
    color_bar = ColorBar(color_mapper=color_mapper,
                         label_standoff=8,
                         width=30,
                         height=600,
                         location=(0, 0),
                         orientation='vertical',
                         background_fill_color='#2f2f2f',
                         major_label_text_color='#ffffff')

    p = figure(title='Nombres d\'hospitalisations mensuelles depuis Mars 2020',
               plot_height=650, plot_width=1000,
               toolbar_location=None,
               output_backend="webgl")
    p.xgrid.grid_line_color = None
    p.ygrid.grid_line_color = None
    p.axis.visible = False
    p.patches('xs', 'ys', source=geosource, fill_color={'field': field_name, 'transform': color_mapper},
              line_color='black', line_width=0.25, fill_alpha=1)
    p.add_layout(color_bar, 'right')
    p.add_tools(hover)
    return p


input_field = 'nb_patients_hosp'

    
hover = HoverTool(tooltips=[('Nom', '@nom'),
                            ('Hospitalisations', '@nb_patients_hosp')])

slider = Slider(title='Mois', start=0, end=len(dates), step=1, value=0)
slider.on_change('value', update_plot)
geojson, df_yr = json_data(1)
geosource = GeoJSONDataSource(geojson=geojson)

p = make_plot(input_field, df_yr)
div = Div(text="24 Janvier 2020 - Premiers cas de covid confirmès en France ")
btn1 = Button(label="Mars 2020 - Mise en place du premier confinement", width=400)
btn2 = Button(label="Fin Mai 2020 - Fin de la phase 2 du déconfinement", width=400)
btn3 = Button(label="Fin Juin 2020 - Phase 3 du déconfinement", width=400)
btn4 = Button(label="30 octobre 2020 - Remise en place du confinement", width=400)
btn5 = Button(label="15 decembre 2020 - Fin de la phase 2 du deconfinement", width=400)
btn6 = Button(label="18 janvier 2021 - Mise en place du couvre feu aprs 18h", width=400)
btn7 = Button(label="3 avril 2021 - 3eme confinement", width=400)
btn8 = Button(label="Juin 2021 - fin du 3eme confinement", width=400)



def set_slider_value(val):
    slider.value = val


btn1.on_click(partial(set_slider_value, val=0))
btn2.on_click(partial(set_slider_value, val=2))
btn3.on_click(partial(set_slider_value, val=3))
btn4.on_click(partial(set_slider_value, val=7))
btn5.on_click(partial(set_slider_value, val=9))
btn6.on_click(partial(set_slider_value, val=10))
btn7.on_click(partial(set_slider_value, val=13))
btn8.on_click(partial(set_slider_value, val=15))


layout = column(p, widgetbox(slider))
layout2 = column(div, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8)
row_layout = Row(layout, layout2)

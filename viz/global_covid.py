import time
import datetime

import bokeh.io.doc
import bokeh.layouts
import bokeh.models
import bokeh.models.tools
import pandas as pd
from bokeh.plotting import figure

import tools.data_provision


def _generate_world_diagram(df_global_covid: pd.DataFrame):
    """

    """
    df_world_covid = df_global_covid[df_global_covid['location'] == 'World']

    p = figure(title="Evolution du covid-19 dans le monde",
               name="world_covid",
               x_axis_type="datetime",
               y_axis_location="right",
               toolbar_location=None)

    p.varea(x=df_world_covid['date'],
            y1=df_world_covid['total_cases'],
            color='navy',
            fill_alpha=0.4,
            y2=0)

    p.line(df_world_covid['date'],
           df_world_covid['total_cases'],
           color="navy",
           line_width=6,
           legend_label="Nombre de cas cumulés")

    p.varea(x=df_world_covid['date'],
            y1=df_world_covid['total_deaths'],
            y2=0,
            color="red")

    p.line(df_world_covid['date'],
           df_world_covid['total_deaths'],
           color="red",
           line_width=1,
           legend_label="Nombre de décès cumulés")

    p.add_tools(bokeh.models.tools.HoverTool(
        tooltips=[
            ('Total Cases:', '@y')
        ]
    ))

    d = figure(title="Evolution du nombre de décès attribués au covid-19 dans le monde",
               name="world_covid_deaths",
               x_axis_type="datetime",
               y_axis_location="right",
               toolbar_location=None)

    d.varea(x=df_world_covid['date'],
            y1=df_world_covid['total_deaths'],
            y2=0,
            fill_alpha=0.4,
            color="red")

    d.line(df_world_covid['date'],
           df_world_covid['total_deaths'],
           color="red",
           line_width=5,
           legend_label="Nombre de décès cumulés")

    d.add_tools(bokeh.models.tools.HoverTool(
        tooltips=[
            ('Total Deaths:', '@y')
        ]
    ))

    p.yaxis.formatter.use_scientific = False
    d.yaxis.formatter.use_scientific = False

    p.xaxis.axis_label = "Date"
    p.yaxis.axis_label = "Nombre de personnes concernées"

    d.xaxis.axis_label = "Date"
    d.yaxis.axis_label = "Nombre de décès dus au covid-19"

    p.legend.location = "top_center"
    d.legend.location = "top_center"

    return bokeh.layouts.column(p, d, sizing_mode="stretch_width")


def _generate_country_diagrams(df_global_covid: pd.DataFrame, country: str):
    """

    """
    df_country = df_global_covid[df_global_covid['location'] == country]

    fig = bokeh.plotting.figure(
        title=f"Nombre de cas journaliers : {country}",
        name=f"covid_{country.replace(' ', '_').lower()}",
        x_axis_type="datetime",
        toolbar_location=None)

    if country == "France":

        fig.line(df_country['date'],
                 df_country['total_deaths'],
                 color="cyan",
                 line_width=3,
                 legend_label="Nombre de décès cumulés")

        lockdown1 = bokeh.models.Span(location=df_country['date'].iloc[53],
                                      dimension='height',
                                      line_width=3,
                                      line_dash='dashed',
                                      line_color="fuchsia")
        fig.line([], [], color="green", line_width=2, legend_label="Premier confinement")

        lockdown2 = bokeh.models.Span(location=df_country['date'].iloc[280],
                                      dimension='height',
                                      line_width=3,
                                      line_dash='dashed',
                                      line_color="yellow")
        fig.line([], [], color="yellow", line_width=2, legend_label="Second confinement")

        lockdown3 = bokeh.models.Span(location=df_country['date'].iloc[435],
                                      dimension='height',
                                      line_width=3,
                                      line_dash='dashed',
                                      line_color="orange")
        fig.line([], [], color="orange", line_width=2, legend_label="Troisième confinement")

        fig.add_layout(lockdown1)
        fig.add_layout(lockdown2)
        fig.add_layout(lockdown3)

    fig.line(df_country['date'],
             df_country['new_cases'],
             color="red",
             line_width=2,
             legend_label="Nombre de nouveaux cas journaliers")

    fig.add_tools(bokeh.models.tools.HoverTool(
        tooltips=[
            ('Nouveaux cas:', '@y')
        ]
    ))

    fig.yaxis.formatter.use_scientific = False

    fig.xaxis.axis_label = "Date"
    fig.yaxis.axis_label = "Nombre de personnes concernées"

    return fig


def generate_global_diagrams():
    """

    """
    global_covid_provider = tools.data_provision.DataProvider("global_covid")
    df_global_covid = global_covid_provider.load_cleaned_pandas_dataset()

    df_global_covid['date'] = pd.to_datetime(df_global_covid['date'])

    cov_fig_ru = _generate_country_diagrams(df_global_covid, "China")
    cov_fig_zh = _generate_country_diagrams(df_global_covid, "Russia")
    cov_fig_in = _generate_country_diagrams(df_global_covid, "India")

    cov_fig_fr = _generate_country_diagrams(df_global_covid, "France")
    cov_fig_uk = _generate_country_diagrams(df_global_covid, "United Kingdom")
    cov_fig_us = _generate_country_diagrams(df_global_covid, "United States")

    cov_fig_br = _generate_country_diagrams(df_global_covid, "Brazil")
    cov_fig_eg = _generate_country_diagrams(df_global_covid, "Egypt")

    world_layout = _generate_world_diagram(df_global_covid)

    # countries_layout = bokeh.layouts.layout(
    #     [
    #         [cov_fig_ru, cov_fig_zh, cov_fig_in],
    #         [cov_fig_us, cov_fig_uk],
    #         [cov_fig_eg, cov_fig_br],
    #         [cov_fig_fr]
    #     ],
    #     height=80,
    #     sizing_mode="stretch_width",
    #     name="covid_countries"
    # )

    # return bokeh.layouts.column(world_layout, countries_layout)

    return [world_layout, cov_fig_ru, cov_fig_zh, cov_fig_in, cov_fig_us, cov_fig_uk, cov_fig_eg, cov_fig_br, cov_fig_fr]

from bokeh.layouts import column
from bokeh.models import ColumnDataSource, FactorRange
from bokeh.plotting import figure
from bokeh.transform import factor_cmap

import tools.data_provision


def reformat_data(df):
    columns = df.columns.to_list()
    columns_values = {}
    for i in range(len(columns)):
        columns_values[columns[i]] = df[columns[i]].to_list()

    return columns_values


def reformat_dico(df):
    age = df["Age"].to_list()
    nb_emploi = df["Nombre de personnes en emploi"].to_list()
    nb_heure_moyen = df["Nombre moyen d’heures travaillées"].to_list()
    dico = {}
    for i in range(len(age)):
        row = [nb_emploi[i], nb_heure_moyen[i]]
        dico[age[i]] = row
    return dico


def show_bar():
    heure_diff = tools.data_provision.DataProvider('heureDiff', "xlsx").load_cleaned_pandas_dataset()
    data = reformat_data(heure_diff)
    dico = reformat_dico(heure_diff)

    keys = list(data.keys())
    ages = keys[1:]
    categories = data[keys[0]]

    palette = ["#c9d9d3", "#718dbf", "#e84d60"]

    x = [(age, categorie) for age in ages for categorie in categories]
    counts = sum(zip(dico[categories[0]], dico[categories[1]], dico[categories[2]]), ())  # like an hstack

    source = ColumnDataSource(data=dict(x=x, counts=counts))

    p = figure(x_range=FactorRange(*x), height=500, title="Statistique de l'embauche en % selon les tranches d'age ",
               toolbar_location=None, tools="")

    p.vbar(x='x', top='counts', width=0.9, source=source, line_color="white",
           fill_color=factor_cmap('x', palette=palette, factors=categories, start=1, end=2))

    p.y_range.start = -10
    p.x_range.range_padding = 1
    p.xaxis.major_label_orientation = 1
    p.xgrid.grid_line_color = None

    return p


def print_heure_diff() -> figure:
    p = show_bar()
    return p

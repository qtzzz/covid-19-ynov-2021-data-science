import urllib.request
from tools import s3
import cleaning.chomage
import cleaning.heure_diff
import cleaning.cleaning

links = {
    'global_data.csv': 'https://covid19.who.int/WHO-COVID-19-global-data.csv',
    'latest.csv': 'https://covid19.who.int/WHO-COVID-19-global-table-data.csv',
    'global_covid.csv': 'https://raw.githubusercontent.com/owid/covid-19-data/master/public/data/owid-covid-data.csv',
    'covid_departement.csv': 'https://www.data.gouv.fr/fr/datasets/r/5c4e1452-3850-4b59-b11c-3dd51d7fb8b5',
    'hospitalisation_deces_age.xlsx': 'https://filedn.com/lEm3p054jseYU0nKqClUIoF/Robin/DataCovid/HospitalisationDecesAge.xlsx',
    'chomage.xlsx': 'https://www.insee.fr/fr/statistiques/fichier/2532173/econ-gen-taux-cho-trim-2.xlsx',
    'heureDiff.xlsx': 'https://www.insee.fr/fr/statistiques/fichier/5432513/FPORSOC21_E5.xlsx'
}

for dataset_name, dataset_url in links.items():
    print(dataset_url)
    req = urllib.request.Request(dataset_url)
    req.add_header('User-Agent', 'Mozilla/5.0')
    result = urllib.request.urlopen(req)
    s3.upload_object("raw/" + dataset_name, result.read())

cleaning.chomage.chomage_clean()
cleaning.heure_diff.clean_heure_diff()
cleaning.cleaning.clean_covid_global()
cleaning.cleaning.clean_covid_france_departement()
cleaning.cleaning.clean_age_hospitalisation_deces()
cleaning.cleaning.clean_age_syndrom_depress()
cleaning.cleaning.clean_confiance_avenir_age()
cleaning.cleaning.clean_garanti_jeune()
cleaning.cleaning.clean_aides_sociales()

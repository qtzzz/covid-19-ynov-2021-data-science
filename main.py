import bokeh.io.doc
import bokeh.layouts

import depression
import heure_diff_chart
import viz.choropleth as choro
import viz.global_covid

# global_cov_layout = viz.global_covid.generate_global_diagrams()
global_cov = viz.global_covid.generate_global_diagrams()

dep = depression.print_heure_diff()
heure = heure_diff_chart.print_heure_diff()

main_layout = bokeh.layouts.column(bokeh.layouts.row(global_cov[0]),
                                   bokeh.layouts.row(global_cov[1], global_cov[2], global_cov[3], sizing_mode="stretch_width"),
                                   bokeh.layouts.row(global_cov[4], global_cov[5], sizing_mode="stretch_width"),
                                   bokeh.layouts.row(global_cov[6], global_cov[7], sizing_mode="stretch_width"),
                                   bokeh.layouts.row(global_cov[8], sizing_mode="stretch_width"),
                                   choro.row_layout,
                                   bokeh.layouts.row(dep, heure),
                                   name="main_layout")

bokeh.io.doc.curdoc().add_root(main_layout)

# bokeh.io.doc.curdoc().add_root(bokeh.layouts.layout(
#     [
#         [[global_cov_layout],
#          [dep, heure],
#          [choro.row_layout]]
#     ],
#     name="main_layout"
# ))

import pandas as pd
import tools.data_provision


def reformat(row):
    """
        Adding sex to age condition
    """
    Femmes = [i for i in range(1, 3)]
    Hommes = [i for i in range(5, 7)]
    Ensemble = [i for i in range(9, 11)]

    if(row.name in Femmes):
        return "Femmes -"+row['sexe_age']
    elif(row.name in Hommes):
        return "Hommes -"+row['sexe_age']
    elif(row.name in Ensemble):
        return "Ensemble -"+row['sexe_age']
    else:
        return row['sexe_age']


def chomage_clean():
    fileChomage = tools.data_provision.DataProvider('chomage', "xlsx")
    # print(fileChomage.load_file())
    dataImport = pd.read_excel(
        fileChomage.load_file(), skiprows=3, skipfooter=5)
    dfChomage = dataImport
    dfChomage.rename(columns={"Sexe et âge": "sexe_age"}, inplace=True)
    dfChomage['sexe_age'] = dfChomage.apply(lambda row: reformat(row), axis=1)
    dfChomage = dfChomage.drop(
        dfChomage.loc[:, "1975-T1":"2017-T4"].columns, axis=1)
    dfChomage = dfChomage.drop(dfChomage.loc[:, "2021-T3":].columns, axis=1)
    fileChomage.save_cleaned_dataset(dfChomage)


if __name__ == '__main__':
    chomage_clean()

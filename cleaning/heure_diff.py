from bokeh.layouts import column
import pandas as pd
import tools.data_provision


def clean_heure_diff():
    """
    Clean
    """
    fileHeureDiff = tools.data_provision.DataProvider('heureDiff', "xlsx")
    dataImport = pd.ExcelFile(fileHeureDiff.load_file())
    dfHeureDiff = pd.read_excel(
        dataImport, "Figure 1", skiprows=2, skipfooter=3)
    dfHeureDiff = dfHeureDiff.drop(
        dfHeureDiff.loc[:, "Nombre total d’heures travaillées":].columns, axis=1)
    dfHeureDiff = dfHeureDiff.dropna()
    dfHeureDiffAge = dfHeureDiff[dfHeureDiff["Unnamed: 0"].str.contains('ans')]
    dfHeureDiffAge = dfHeureDiffAge.rename(columns={'Unnamed: 0': 'Age'})
    dfTrimestrielle = dfHeureDiff[dfHeureDiff["Unnamed: 0"].str.contains(
        'trim.')]
    dfTrimestrielle = dfTrimestrielle.rename(columns={'Unnamed: 0': 'Periode'})
    fileHeureDiff.save_cleaned_dataset(dfHeureDiffAge)


if __name__ == '__main__':
    clean_heure_diff()

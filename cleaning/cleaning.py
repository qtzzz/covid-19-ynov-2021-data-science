import pandas as pd
import requests
import io
import tools.data_provision


def clean_covid_global():
    # DATASET COVID MONDE GLOBAL :
    # Recuperation du dataset en ligne :
    data_provider = tools.data_provision.DataProvider('global_covid')
    chiffreGlobal = data_provider.load_raw_dataset()

    # Traitement/Nettoyage :
    chiffreGlobal = chiffreGlobal[[
        'location', 'date', 'total_cases', 'new_cases', 'total_deaths']]
    chiffreGlobal.date = pd.to_datetime(chiffreGlobal.date, format='%Y-%m-%d')
    chiffreGlobal = chiffreGlobal.fillna(0)

    # Uniquement pays voulu :

    chiffreGlobal = chiffreGlobal[(chiffreGlobal.location == 'France') | (chiffreGlobal.location == 'United States') |
                                  (chiffreGlobal.location == 'China') | (chiffreGlobal.location == 'Russia') |
                                  (chiffreGlobal.location == 'Brazil') | (chiffreGlobal.location == 'United Kingdom') |
                                  (chiffreGlobal.location == 'Egypt') | (chiffreGlobal.location == 'India') |
                                  (chiffreGlobal.location == 'World')
                                  ]

    # Decoupage par groupe
    chiffreCovidPays = chiffreGlobal.groupby('location')
    # chiffreCovidPays.groups.keys()

    # creation de DF unique par pays

    #globalWorld = chiffreCovidPays.get_group('World')
    #globalFrance = chiffreCovidPays.get_group('France')
    #globalUSA = chiffreCovidPays.get_group('United States')
    #globalChina = chiffreCovidPays.get_group('China')
    #globalRussia = chiffreCovidPays.get_group('Russia')
    #globalBrazil = chiffreCovidPays.get_group('Brazil')
    #globalUK = chiffreCovidPays.get_group('United Kingdom')
    #globalEgyptia = chiffreCovidPays.get_group('Egypt')
    #globalIndia = chiffreCovidPays.get_group('India')

    data_provider.save_cleaned_dataset(chiffreGlobal)


def clean_covid_france_departement():
    # DATASET COVID FRANCE :

    data_provider = tools.data_provision.DataProvider('covid_departement')
    chiffreCovidFrance = data_provider.load_raw_dataset()
    chiffreCovidFrance.reset_index(level=0, inplace=True)
    chiffreCovidFrance = chiffreCovidFrance[['date', 'dep', 'hosp', 'rad']]
    chiffreCovidFrance = chiffreCovidFrance.rename(
        columns={'dep': 'departement', 'hosp': 'nb_patients_hosp', 'rad': 'total_hosp'})
    chiffreCovidFrance.date = pd.to_datetime(
        chiffreCovidFrance.date, format='%Y-%m-%d')
    chiffreCovidFrance = chiffreCovidFrance.fillna(0)

    data_provider.save_cleaned_dataset(chiffreCovidFrance)


def clean_age_hospitalisation_deces():

    # Creation du df
    # data_provider = tools.data_provision.DataProvider(
    #     'hospitalisation_deces_age')
    # ageDecesHospit = data_provider.load_raw_dataset(fig='Figure 1')
    # print(ageDecesHospit.head())

    data_provider = tools.data_provision.DataProvider(
        'hospitalisation_deces_age', "xlsx")
    dataImport = pd.ExcelFile(data_provider.load_file())
    ageDecesHospit = pd.read_excel(dataImport, 'Figure 1')
    # Tri des lignes avec index remis a 0
    ageDecesHospit = ageDecesHospit.drop([0, 1, 9])
    ageDecesHospit.reset_index(inplace=True)

    # Tri et renommage des colonnes
    ageDecesHospit = ageDecesHospit.rename(
        columns={'Unnamed: 0': 'Age', 'Unnamed: 1': 'Hospitalisation', 'Unnamed: 2': 'Décès'})
    ageDecesHospit = ageDecesHospit[['Age', 'Hospitalisation', 'Décès']]

    # Homogénéisation des valeurs
    ageDecesHospit['Hospitalisation'] = ageDecesHospit['Hospitalisation'].astype(
        float)
    ageDecesHospit['Hospitalisation'] = ageDecesHospit['Hospitalisation'].round(
        0).astype(int)
    ageDecesHospit['Décès'] = ageDecesHospit['Décès'].astype(float)
    ageDecesHospit['Décès'] = ageDecesHospit['Décès'].round(0).astype(int)

    # Sauvegarde :
    data_provider.save_cleaned_dataset(ageDecesHospit)


def clean_age_syndrom_depress():

    data_provider = tools.data_provision.DataProvider(
        'hospitalisation_deces_age', "xlsx")
    dataImport = pd.ExcelFile(data_provider.load_file())
    ageSyndromeDepressif = pd.read_excel(dataImport, 'Figure 2')
    # Creation du df

    # Tri des lignes reset index
    ageSyndromeDepressif = ageSyndromeDepressif.drop([0, 2, 3])
    ageSyndromeDepressif.reset_index(inplace=True)
    ageSyndromeDepressif = ageSyndromeDepressif.transpose()

    # Tri et renommage des colonnes
    ageSyndromeDepressif = ageSyndromeDepressif.rename(
        index=str, columns={0: 'Date', 1: '18-24 ans', 2: '25-29 ans', 3: '30 ans ou plus'})

    ageSyndromeDepressif = ageSyndromeDepressif.drop(
        index=['index', 'Unnamed: 0', 'Unnamed: 2', 'Unnamed: 3', 'Unnamed: 5', 'Unnamed: 6'])
    ageSyndromeDepressif = ageSyndromeDepressif.rename(
        index={'Unnamed: 1': '0', 'Unnamed: 4': '1', 'Unnamed: 7': '2'})

    # Homogénéisation des valeurs
    ageSyndromeDepressif.Date[0] = pd.to_datetime(
        ageSyndromeDepressif.Date[0], format='%Y')
    ageSyndromeDepressif.Date[[1, 2]] = pd.to_datetime(
        ageSyndromeDepressif.Date[[1, 2]], format='%Y-%m')
    ageSyndromeDepressif.Date = pd.to_datetime(ageSyndromeDepressif.Date)
    ageSyndromeDepressif[['18-24 ans', '25-29 ans', '30 ans ou plus']
                         ] = ageSyndromeDepressif[['18-24 ans', '25-29 ans', '30 ans ou plus']].astype(float)

    # Sauvegarde :
    data_provider.save_cleaned_dataset(ageSyndromeDepressif)


def clean_confiance_avenir_age():
    # Creation du df
    data_provider = tools.data_provision.DataProvider(
        'hospitalisation_deces_age', "xlsx")
    dataImport = pd.ExcelFile(data_provider.load_file())
    confianceAvenirAge = pd.read_excel(dataImport, 'Figure 3')

    # Tri des lignes reset index
    confianceAvenirAge = confianceAvenirAge.drop([0, 1])
    confianceAvenirAge.reset_index(inplace=True)

    # Tri et renommage des colonnes
    confianceAvenirAge = confianceAvenirAge.rename(
        columns={'Unnamed: 0': 'Date', 'Unnamed: 1': '18_29ans', 'Unnamed: 2': '30_59ans'})
    confianceAvenirAge = confianceAvenirAge[['Date', '18_29ans', '30_59ans']]

    # Homogénéisation des valeurs
    confianceAvenirAge.Date = pd.to_datetime(
        confianceAvenirAge.Date, format='%Y')
    confianceAvenirAge[['18_29ans', '30_59ans']] = confianceAvenirAge[[
        '18_29ans', '30_59ans']].astype(float)

    # Sauvegarde :
    data_provider.save_cleaned_dataset(confianceAvenirAge)


def clean_garanti_jeune():
    # Creation du df
    data_provider = tools.data_provision.DataProvider(
        'hospitalisation_deces_age', "xlsx")
    dataImport = pd.ExcelFile(data_provider.load_file())
    chiffreGarantiJeune = pd.read_excel(dataImport, 'Figure 5')

    # Tri des lignes reset index
    chiffreGarantiJeune = chiffreGarantiJeune.drop([0])
    chiffreGarantiJeune.reset_index(inplace=True)

    # Tri et renommage des colonnes
    chiffreGarantiJeune = chiffreGarantiJeune.rename(
        columns={'Unnamed: 0': 'Date', 'Unnamed: 1': 'Inscriptions'})
    chiffreGarantiJeune = chiffreGarantiJeune[['Date', 'Inscriptions']]

    # Homogénéisation des valeurs
    chiffreGarantiJeune.Date = pd.to_datetime(
        chiffreGarantiJeune.Date, format='%Y-%m')
    chiffreGarantiJeune['Inscriptions'] = chiffreGarantiJeune['Inscriptions'].astype(
        int)

    # Sauvegarde :
    data_provider.save_cleaned_dataset(chiffreGarantiJeune)


def clean_aides_sociales():
    # Creation du df
    data_provider = tools.data_provision.DataProvider(
        'hospitalisation_deces_age', "xlsx")
    dataImport = pd.ExcelFile(data_provider.load_file())
    aidesSocialesJeune = pd.read_excel(dataImport, 'Figure 6')

    # Tri et renommage des colonnes
    aidesSocialesJeune = aidesSocialesJeune.rename(
        columns={'Unnamed: 0': 'Date', 'Unnamed: 1': 'Inscriptions'})
    aidesSocialesJeune = aidesSocialesJeune[['Date', 'Inscriptions']]

    # Homogénéisation des valeurs
    aidesSocialesJeune.Date = pd.to_datetime(
        aidesSocialesJeune.Date, format='%Y-%m')

    # Sauvegarde :
    data_provider.save_cleaned_dataset(aidesSocialesJeune)


if __name__ == '__main__':
    clean_covid_global()
    clean_covid_france_departement()
    clean_age_hospitalisation_deces()
    clean_age_syndrom_depress()
    clean_confiance_avenir_age()
    clean_garanti_jeune()
    clean_aides_sociales()

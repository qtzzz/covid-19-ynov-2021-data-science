FROM python:3.9

ENV PORT=8080
ENV BOKEH_ALLOW_WS_ORIGIN='dataviz-36pscew4va-ew.a.run.app'
WORKDIR /app
COPY . /app
RUN pip install -r requirements.txt

CMD ["bokeh", "serve",".", "--port", "8080"]
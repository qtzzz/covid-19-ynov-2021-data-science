# Import libraries
import pandas as pd
import numpy as np
import math

import geopandas as gpd
import json
import tools.data_provision
from bokeh.io import output_notebook, show, output_file
from bokeh.plotting import figure
from bokeh.models import GeoJSONDataSource, LinearColorMapper, ColorBar, NumeralTickFormatter
from bokeh.palettes import brewer

from bokeh.io.doc import curdoc
from bokeh.models import Slider, HoverTool, Select, DateSlider
from bokeh.layouts import widgetbox, row, column

data_provider = tools.data_provision.DataProvider('covid_departement')
df_dept = data_provider.load_cleaned_pandas_dataset()
gdf = gpd.read_file("./assets/departements-20180101.shp")
gdf.drop(gdf.index[gdf['code_insee'] == '972'], inplace=True)
gdf.drop(gdf.index[gdf['code_insee'] == '971'], inplace=True)
gdf.drop(gdf.index[gdf['code_insee'] == '973'], inplace=True)
gdf.drop(gdf.index[gdf['code_insee'] == '974'], inplace=True)
gdf.drop(gdf.index[gdf['code_insee'] == '975'], inplace=True)
gdf.drop(gdf.index[gdf['code_insee'] == '976'], inplace=True)
df_dept.drop(df_dept.index[df_dept['departement'] == '972'], inplace=True)
df_dept.drop(df_dept.index[df_dept['departement'] == '971'], inplace=True)
df_dept.drop(df_dept.index[df_dept['departement'] == '973'], inplace=True)
df_dept.drop(df_dept.index[df_dept['departement'] == '974'], inplace=True)
df_dept.drop(df_dept.index[df_dept['departement'] == '975'], inplace=True)
df_dept.drop(df_dept.index[df_dept['departement'] == '976'], inplace=True)
# gdf['x'] = [geometry.x for geometry in gdf['geometry']]
# gdf['y'] = [geometry.y for geometry in gdf['geometry']]
df_dept = df_dept.rename(columns={'departement': 'code_insee'})
df_dept['date'] = pd.to_datetime(df_dept['date'])
# df_dept = df_dept.resample('code_insee', pd.Grouper(key='date', freq='M'))\
#     .agg('sum')
# df_dept = df_dept.groupby(df_dept['date'])['M'].sum().reset_index()
df_dept['date'] = pd.to_datetime(df_dept.date).dt.to_period('m').dt.strftime('%Y-%m')
df_dept = df_dept.groupby(['date', 'code_insee'])[['nb_patients_hosp']].sum().reset_index()

# gdf = gdf.merge(df_dept, on='code_insee')
# geosource = gdf.to_json()


# Create a function the returns json_data for the year selected by the user
def json_data(selectedYear):

    yr = selectedYear
    # Pull selected year from neighborhood summary data
    df_yr = df_dept[df_dept['date'] == yr]

    # Merge the GeoDataframe object (sf) with the neighborhood summary data (neighborhood)
    merged = gdf.merge(df_yr, on='code_insee')

    # Bokeh uses geojson formatting, representing geographical features, with json
    # Convert to json
    merged_json = json.loads(merged.to_json())

    # Convert to json preferred string-like object
    json_data = json.dumps(merged_json)
    return json_data


def update_plot(attr, old, new):
    # The input yr is the year selected from the slider
    yr = slider.value
    new_data = json_data(yr)

    # Update the plot based on the changed inputs
    p = make_plot(input_field)
    # Update the layout, clear the old document and display the new document
    layout = column(p, widgetbox(slider))
    curdoc().clear()
    curdoc().add_root(layout)

    # Update the data
    geosource.geojson = new_data


def make_plot(field_name):

    color_mapper = LinearColorMapper(palette=palette, low=df_dept['nb_patients_hosp'].min(),
                                     high=df_dept['nb_patients_hosp'].max())
    color_bar = ColorBar(color_mapper=color_mapper,
                         label_standoff=8,
                         width=500,
                         height=20,
                         location=(0, 0),
                         orientation='horizontal')

    p = figure(title='Covid fr metropo',
               plot_height=650, plot_width=850,
               toolbar_location=None)
    p.xgrid.grid_line_color = None
    p.ygrid.grid_line_color = None
    p.axis.visible = False

    # Add patch renderer to figure.
    p.patches('xs', 'ys', source=geosource, fill_color={'field': field_name, 'transform': color_mapper},
              line_color='black', line_width=0.25, fill_alpha=1)

    # Specify color bar layout.
    p.add_layout(color_bar, 'right')

    # Add the hover tool to the graph
    p.add_tools(hover)
    return p


geosource = GeoJSONDataSource(geojson=json_data('2019-03'))

input_field = 'nb_patients_hosp'

# Define a sequential multi-hue color palette.
palette = brewer['Blues'][8]

# Reverse color order so that dark blue is highest obesity.
palette = palette[::-1]

# Add hover tool
hover = HoverTool(tooltips=[('Nom', '@nom'),
                            ('Hospitalisations', '@nb_patients_hosp')])

# Call the plotting function
p = make_plot(input_field)

# Make a slider object: slider

dates = df_dept['date'].unique()
dates = dates.astype(np.datetime64)

slider = DateSlider(title='Month', start=dates[0], end=dates[-1], step=1, value=dates[0])
slider.on_change('value', update_plot)


# Make a column layout of widgetbox(slider) and plot, and add it to the current document
# Display the current document
layout = column(p, widgetbox(slider))
curdoc().add_root(layout)

# Use the following code to test in a notebook, comment out for transfer to live site
# Interactive features will not show in notebook
# output_notebook()
# show(p)
